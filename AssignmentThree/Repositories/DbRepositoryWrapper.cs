﻿using AssignmentThree.Data;
using AssignmentThree.Repositories.Base;
using Microsoft.EntityFrameworkCore;

namespace AssignmentThree.Repositories
{
    public class DbRepositoryWrapper : IRepositoryWrapper
    {
        private MovieDbContext _context { get; }
        private ICharacterRepository _characterRepository;
        private IFranchiseRepository _franchiseRepository;
        private IMovieRepository _movieRepository;

        public DbRepositoryWrapper(MovieDbContext context)
        {
            _context = context ?? throw new ArgumentNullException("Failed to initialize MovieDbContext.");
        }
        public ICharacterRepository Characters
        {
            get
            {
                if (_characterRepository is null)
                {
                    _characterRepository = new DbCharacterRepository(_context);
                }
                return _characterRepository;
            }
        }

        public IFranchiseRepository Franchises
        {
            get
            {
                if (_franchiseRepository is null)
                {
                    _franchiseRepository = new DbFranchiseRepository(_context);
                }
                return _franchiseRepository;
            }
        }
        public IMovieRepository Movies
        {
            get
            {
                if (_movieRepository is null)
                {
                    _movieRepository = new DbMovieRepository(_context);
                }
                return _movieRepository;
            }
        }


        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
