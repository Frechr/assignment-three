﻿using AssignmentThree.Repositories.Base;

namespace AssignmentThree.Repositories
{
    public interface IRepositoryWrapper 
    {
        ICharacterRepository Characters { get; }
        IFranchiseRepository Franchises { get; }
        IMovieRepository Movies { get; }
        void Save();
    }
}
