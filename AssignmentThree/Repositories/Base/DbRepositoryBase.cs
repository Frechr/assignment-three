﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using AssignmentThree.Data;
using System.Data.SqlTypes;
using System.Reflection.Metadata.Ecma335;

namespace AssignmentThree.Repositories.Base
{
    public class DbRepositoryBase<T> : IRepository<T> where T : class
    {
        protected MovieDbContext repositoryContext { get; set; }

        public DbRepositoryBase(MovieDbContext repositoryContext)
        {
            this.repositoryContext = repositoryContext;
        }
        /// <summary>
        /// Gets all entities (untracked, changes must be called with Save() ).
        /// </summary>
        /// <returns>The collection of <see cref="T"/></returns>
        public IQueryable<T> GetAll() => repositoryContext.Set<T>();
        /// <summary>
        /// Gets the specified entity (tracked, changes are saved automatically).
        /// </summary>
        /// <param name="Id">The identifier.</param>
        /// <returns></returns>
        public T Get(int Id)=> repositoryContext.Set<T>().Find(Id);
        public bool Save()=>repositoryContext.SaveChanges() != 0;

        /// <summary>
        /// Creates the specified entity.
        /// <inheritdoc cref="Microsoft.EntityFrameworkCore.DbContext.Add(object)"/>
        /// </summary>
        /// <param name="entity">The entity.</param>
        public bool Create(T entity) { var result = repositoryContext.Set<T>().Add(entity); Save(); return result is not null; }
        public bool Update(T entity) { var result = repositoryContext.Set<T>().Update(entity); Save(); return result is not null; }
        public bool Delete(T entity) { var result = repositoryContext.Set<T>().Remove(entity); Save(); return result is not null; }
    }
}
