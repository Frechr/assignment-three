﻿using AssignmentThree.Data;
using AssignmentThree.Models;
using Microsoft.EntityFrameworkCore;

namespace AssignmentThree.Repositories.Base
{
    public class DbFranchiseRepository : DbRepositoryBase<Franchise>, IFranchiseRepository
    {
        public DbFranchiseRepository(MovieDbContext context) : base(context)
        {
        }
    }
}
