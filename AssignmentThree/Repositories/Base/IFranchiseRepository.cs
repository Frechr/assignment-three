﻿using AssignmentThree.Models;

namespace AssignmentThree.Repositories.Base
{
    public interface IFranchiseRepository : IRepository<Franchise>
    {
    }
}