﻿using AssignmentThree.Data;
using AssignmentThree.Models;
using Microsoft.EntityFrameworkCore;

namespace AssignmentThree.Repositories.Base
{
    public class DbCharacterRepository : DbRepositoryBase<Character>, ICharacterRepository
    {
        public DbCharacterRepository(MovieDbContext context) : base(context)
        {

        }
    }
}
