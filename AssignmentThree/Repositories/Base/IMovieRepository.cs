﻿using AssignmentThree.Models;

namespace AssignmentThree.Repositories.Base
{
    public interface IMovieRepository : IRepository<Movie>
    {
        public bool UpdateCharacters(int movieIdentifier, List<Character> characterIdentifiers);
    }
}