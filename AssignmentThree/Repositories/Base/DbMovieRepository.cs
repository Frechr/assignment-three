﻿using AssignmentThree.Models;
using AssignmentThree.Data;
using Microsoft.EntityFrameworkCore;

namespace AssignmentThree.Repositories.Base
{
    public class DbMovieRepository : DbRepositoryBase<Movie>, IMovieRepository
    {
        public DbMovieRepository(MovieDbContext context) : base(context)
        {
        }
        public bool UpdateCharacters(int movieId, List<Character> characters)
        {
            var movieToUpdate = Get(movieId);
            repositoryContext.Add(movieToUpdate);
            repositoryContext.Add(characters);
            movieToUpdate.Characters = characters;
            return Update(movieToUpdate);
        }
    }
}
