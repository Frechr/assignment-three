﻿using AssignmentThree.Models;

namespace AssignmentThree.Repositories.Base
{
    public interface ICharacterRepository : IRepository<Character>
    {
    }
}