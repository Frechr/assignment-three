﻿using System.Linq.Expressions;

namespace AssignmentThree.Repositories.Base
{
    public interface IRepository<T>
    {
        IQueryable<T> GetAll();
        T Get(int Id);
        bool Create(T entity);
        bool Update(T entity);
        bool Delete(T entity);
    }
}
