﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AssignmentThree.Data;
using AssignmentThree.Models;
using AssignmentThree.Repositories;
using AutoMapper;
using AssignmentThree.Models.DTO;
using System.Net.Mime;

namespace AssignmentThree.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchisesController : ControllerBase
    {
        private readonly IRepositoryWrapper _context;
        private readonly IMapper _mapper;

        public FranchisesController(IRepositoryWrapper context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Franchises
        [HttpGet]
        public ActionResult<IEnumerable<FranchiseDTO>> GetFranchises()
        {
            var franchises = _context.Franchises.GetAll()?.ToList() ?? new List<Franchise>();
            return franchises.Select(franchise=> _mapper.Map<FranchiseDTO>(franchise)).ToList();
        }

        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public ActionResult<FranchiseDTO> GetFranchise(int id)
        {
            var franchise = _context.Franchises.Get(id);

            if (franchise is null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseDTO>(franchise);
        }

        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public IActionResult PutFranchise(int id, FranchiseDTO franchise)
        {
            if(!FranchiseExists(id))
                return NotFound();
            if (franchise is null)
            {
                return BadRequest();
            }
            _context.Franchises.Update(_mapper.Map<Franchise>(franchise));

            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public ActionResult<FranchiseDTO> PostFranchise(FranchiseDTO franchise)
        {
            _context.Franchises.Create(_mapper.Map<Franchise>(franchise)); 

            return CreatedAtAction("GetFranchise", franchise);
        }

        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public IActionResult DeleteFranchise(int id)
        {
            var franchise = _context.Franchises.Get(id);
            if (franchise == null)
            {
                return NotFound();
            }
            _context.Franchises.Delete(franchise); 
            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Get(id) is not null;
        }
    }
}
