﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AssignmentThree.Data;
using AssignmentThree.Models;
using AssignmentThree.Repositories;
using AssignmentThree.Models.DTO;
using AutoMapper;
using System.Net.Mime;

namespace AssignmentThree.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MoviesController : ControllerBase
    {
        private readonly IRepositoryWrapper _context;
        private readonly IMapper _mapper;

        public MoviesController(IRepositoryWrapper context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Movies
        [HttpGet]
        public ActionResult<IEnumerable<MovieDTO>> GetMovies()
        {
            var movies = _context.Movies.GetAll()?.ToList() ?? new List<Movie>();
            return movies.Select(movie => _mapper.Map<MovieDTO>(movie)).ToList();
        }

        // GET: api/Movies/5
        [HttpGet("{id}")]
        public ActionResult<MovieDTO> GetMovie(int id)
        {
            var movie = _context.Movies.Get(id);

            if (movie is null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieDTO>(movie);
        }

        // GET: api/Movies/5/characters
        [HttpGet("{id}/characters")]
        public ActionResult<List<CharacterDTO>> GetMovieCharacters(int id)
        {
            var movie = _context.Movies.GetAll().Include(m=>m.Characters).Where(m=>m.Id == id).FirstOrDefault();
            if (movie is null)
            {
                return NotFound();
            }
            if (movie.Characters is null)
            {   
                return Problem(statusCode:500, detail:"Movie has no characters?");
            }
            var characters = movie.Characters;
            var characterDTOs = characters.Select(c=>_mapper.Map<CharacterDTO>(c)).ToList(); 
            return characterDTOs;
        }

        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public IActionResult PutMovie(int id, MovieDTO movie)
        {
            if (!MovieExists(id))
                return NotFound();
            if (movie is null)
            {
                return BadRequest();
            }
            _context.Movies.Update(_mapper.Map<Movie>(movie));

            return NoContent();
        }

        // PUT: api/Movies/5/characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}/characters")]
        public IActionResult PutMovieCharacters(int movieId, int[] characterIdentifiers)
        {
            if (!MovieExists(movieId))
                return NotFound();
            if (characterIdentifiers is null)
                return BadRequest();
            var movie = _context.Movies.GetAll().Where(movie=>movie.Id == movieId).Include(movie=>movie.Characters).SingleOrDefault();
            var newCharacters = _context.Characters.GetAll().Where(character=>characterIdentifiers.Contains(character.Id)).ToList();
            movie.Characters = newCharacters;
            _context.Save();
            return NoContent();
        }

        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public ActionResult<MovieDTO> PostMovie(MovieDTO movie)
        {
            _context.Movies.Create(_mapper.Map<Movie>(movie));

            return CreatedAtAction("GetMovie", movie);
        }

        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public IActionResult DeleteMovie(int id)
        {
            var movie = _context.Movies.Get(id);
            if (movie == null)
            {
                return NotFound();
            }
            _context.Movies.Delete(movie);
            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Get(id) is not null;
        }
    }
}
