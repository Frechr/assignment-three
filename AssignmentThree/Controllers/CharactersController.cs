﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AssignmentThree.Models;
using AssignmentThree.Data;
using AssignmentThree.Repositories;
using AutoMapper;
using AssignmentThree.Models.DTO;
using System.Net.Mime;

namespace AssignmentThree.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {
        private readonly IRepositoryWrapper _context;
        private readonly IMapper _mapper;
        public CharactersController(IRepositoryWrapper context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Characters
        [HttpGet]
        [ProducesResponseType(200)]  
        public ActionResult<IEnumerable<CharacterDTO>> GetCharacters()
        {
            var characters = _context.Characters.GetAll();
            var charactersDTO = characters.Select(character => _mapper.Map<CharacterDTO>(character));
            return charactersDTO.ToList();
        }

        // GET: api/Characters/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<CharacterDTO> GetCharacter(int id)
        {
            var character = _context.Characters.Get(id);

            if (character is null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterDTO>(character);
        }

        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult PutCharacter(int id, CharacterDTO character)
        {
            if (!CharacterExists(id))
            {
                return NotFound();
            }
            if ( character is null)
            {
                return BadRequest();
            }
            _context.Characters.Update(_mapper.Map<Character>(character));

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Character> PostCharacter(CharacterDTO character)
        {
            if (character is null)
            {
                return BadRequest("Character is null.");
            }
            _context.Characters.Create(_mapper.Map<Character>(character));

            return CreatedAtAction("GetCharacter", character);
        }

        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult DeleteCharacter(int id)
        {
            var character = _context.Characters.Get(id);
            if (!CharacterExists(id))
            {
                return NotFound();
            }
            _context.Characters.Delete(character);
            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Get(id) is not null;
        }
    }
}
