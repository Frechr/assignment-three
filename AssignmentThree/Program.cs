using Microsoft.EntityFrameworkCore;
using AssignmentThree.Models;
using AssignmentThree.Data;
using System.Text.Json.Serialization;
using AssignmentThree.Repositories;
using System.Runtime.CompilerServices;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.


builder.Services.AddDbContext<MovieDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("MOVIE_DB")));

builder.Services.AddScoped<IRepositoryWrapper, DbRepositoryWrapper>();

builder.Services.AddAutoMapper(typeof(Program).Assembly);
builder.Services.AddControllers().
    AddJsonOptions(x=>x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options=>options.SwaggerDoc("v1",new Microsoft.OpenApi.Models.OpenApiInfo
{
    Title="MovieDB API",
    Version = "v1",
    Description="A simple ASP.NET WEB API for managing movie details.",
    Contact = new OpenApiContact{
        Name = "Jacob Tornes, Fredrik Christensen"
    },

}));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
