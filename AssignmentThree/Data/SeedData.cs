﻿using AssignmentThree.Models;

namespace AssignmentThree.Data
{
    public class SeedData
    {
        //3 movies, any amount of characters/franchises
        private static List<Movie> _movies= new List<Movie>
            {
                new Movie
                {
                    Id= 1,
                    MovieTitle = "Pirates of the Carribean",
                    Genre = "Fantsy, Adventure, Action",
                    ReleaseYear = new DateTime(2003, 1,1),
                    Director = "Joachim Rønning",
                    Picture = "https://prod-ripcut-delivery.disney-plus.net/v1/variant/disney/C6AB0EDCE8F41882EBBB782B76DD4F05D7E360D7C3F23B4F6D02C24699B26105/scale?width=1200&aspectRatio=1.78&format=jpeg",
                    Trailer = "https://www.youtube.com/watch?v=LkWQvzrv6gI" ,
                    FranchiseID = 1
                },
                new Movie
                {
                    Id= 2,
                    MovieTitle = "Batman",
                    Genre="Action",
                    ReleaseYear= new DateTime(2022, 1, 1),
                    Director = "Matt Reeves" ,
                    FranchiseID = 2
                },
                new Movie{
                    Id= 3,
                    MovieTitle = "Whatever",
                    Genre="Idk",
                    ReleaseYear= new DateTime(2000,1,1),
                    Director="Someone" ,
                    FranchiseID = 3
                }
            };
        private static List<Character> _characters = new List<Character>
            {
                new Character
                {
                    Id= 1,
                    FullName = "Jack Sparrow",
                    Alias   = "Worst pirate in the world",
                    Gender = "Male",
                    Picture = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSCELxs9kucYga4i_DYQ0h7mklSRR8-BI0Gl-c2lHLP1w&s"
                },
                new Character
                {
                    Id= 2,
                    FullName = "Bruce Wayne",
                    Alias   = "The batman!",
                    Gender = "Male",
                    Picture = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Batman_%28black_background%29.jpg/640px-Batman_%28black_background%29.jpg"
                },
                new Character
                {
                    Id=3,
                    FullName = "Test character",
                    Alias   = "Test alias",
                    Gender = "?",
                    Picture = "https://img.freepik.com/free-vector/oops-404-error-with-broken-robot-concept-illustration_114360-5529.jpg?w=2000"
                }
            };
        private static List<Franchise> _franchises = new List<Franchise> {
                new Franchise
                {
                    Id=1,
                    Name = "Disney",
                    Description = "..."
                },
                 new Franchise
                {
                    Id=2,
                    Name = "DC Universe",
                    Description = " ganger møtes."
                },
                  new Franchise
                {
                    Id=3,
                    Name = "SampleFranchise",
                    Description = "Sampletext."
                }
            };
        public static List<Movie> GetMovieSeedData()
        {
            List<Movie> movieWithRelations = _movies; 
            return movieWithRelations;
        } 

        public static List<Character> GetCharacterSeedData()
        {
            return _characters;
        }

        public static List<Franchise> GetFranchiseSeedData()
        {
            return _franchises;
        }
    }
}
