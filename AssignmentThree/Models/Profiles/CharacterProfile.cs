﻿using AssignmentThree.Models.DTO;
using AutoMapper;

namespace AssignmentThree.Models.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterDTO>();
        }
    }
}
