﻿using AssignmentThree.Models.DTO;
using AutoMapper;

namespace AssignmentThree.Models.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseDTO>();
        }
    }
}
