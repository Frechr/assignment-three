﻿using AssignmentThree.Models;
using System.ComponentModel.DataAnnotations;

namespace AssignmentThree.Models.DTO
{
    public class MovieDTO
    {
        /// <summary>
        /// Gets or sets the movie title.
        /// </summary>
        /// <value>
        /// The movie title.
        /// </value>
        [MaxLength(50)]
        public string MovieTitle { get; set; }

        /// <summary>
        /// Gets or sets the genre.
        /// </summary>
        /// <value>
        /// The genre.
        /// </value>
        [MaxLength(32)]
        public string Genre { get; set; }

        /// <summary>
        /// Gets or sets the release year.
        /// </summary>
        /// <value>
        /// The release year.
        /// </value>
        public DateTime ReleaseYear { get; set; }

        /// <summary>
        /// Gets or sets the director.
        /// </summary>
        /// <value>
        /// The director.
        /// </value>
        [MaxLength(50)]
        public string Director { get; set; }

        /// <summary>
        /// Gets or sets the picture.
        /// </summary>
        /// <value>
        /// The picture.
        /// </value>
        public string? Picture { get; set; }

        /// <summary>Gets or sets the trailer.</summary>
        /// <value>The trailer.</value>
        public string? Trailer { get; set; }

        /// <summary>
        /// Foreign key for the franchise.
        /// </summary>
        /// <value>
        /// The franchise identifier.
        /// </value>
        public int? FranchiseID { get; set; }
    }
}
