﻿using System.ComponentModel.DataAnnotations;

namespace AssignmentThree.Models
{
    public class Franchise
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [MaxLength(50)]
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [MaxLength(1000)]
        public string? Description { get; set; } 

        /// <summary>
        /// Navigation property to every movie in a franchise.
        /// </summary>
        public ICollection<Movie>? Movies { get; set; }

    }
}
